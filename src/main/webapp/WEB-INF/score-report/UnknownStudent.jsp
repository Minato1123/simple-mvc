<%-- 
    Document   : UnknownStudent
    Created on : Sep 24, 2018, 7:35:46 AM
    Author     : lendle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            a {
                text-decoration: none;
                background-color: #fab4af;
                color: white;
                padding: 0.5rem;
                box-shadow: 0.1rem 0.1rem 0.2rem 0 rgba(0, 0, 0, 0.3);
                border-radius: 0.5rem;
            }
            
            a:hover {
                background-color: #ff7c73;
            }
            
        </style>
    </head>
    <body>
        <h1>Unknown Student</h1>
        <a href="score">Home Page</a>
    </body>
</html>
