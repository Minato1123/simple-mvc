/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.network.simplemvc;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author lendle
 */
@WebServlet(name = "ShowBalance", urlPatterns = {"/balance"})
public class ShowBalance extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id"); // 得到 id 參數
        if (id == null) { // 如果沒有 id，跳轉到 index.jsp 頁面輸入
            request.getRequestDispatcher("/WEB-INF/bank-account/index.jsp").forward(request, response);
            // 屬於 server side 呼叫
            // 定義在 RequestDispatcher 的介面,由 request.getRequestDispatcher 呼叫
            // 內部轉址，URL 不會更改網址列 （可設定成參數）
            // 因是內部轉址,可以透過 setAttribute 傳遞參數
            // 效率較高
            // 適用於權限管理轉頁時使用
            
            return;
        }
        
        BankCustomer customer=BankCustomer.getCustomer(id);
        String address=null;
        if(customer==null){ // 根據 customer 顯示不同頁面
            address="/WEB-INF/bank-account/UnknownCustomer.jsp";
        }else if(customer.getBalance()<0){
            address="/WEB-INF/bank-account/NegativeBalance.jsp";
            request.setAttribute("customer", customer);
        }else if(customer.getBalance()>10000){
            address="/WEB-INF/bank-account/HighBalance.jsp";
            request.setAttribute("customer", customer);
        }else{
            address="/WEB-INF/bank-account/NormalBalance.jsp";
            request.setAttribute("customer", customer);
        }
        request.getRequestDispatcher(address).forward(request, response);
        // 取得 forward 的物件（內部轉址）
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
